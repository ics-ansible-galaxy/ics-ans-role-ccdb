# ics-ans-role-ccdb

Ansible role to install CCDB.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Role Variables

```yaml
ccdb_network: ccdb-network
ccdb_container_name: ccdb
ccdb_container_image: "{{ ccdb_container_image_name }}:{{ ccdb_container_image_tag }}"
ccdb_container_image_name: registry.esss.lu.se/ics-software/ccdb
ccdb_container_image_tag: latest
ccdb_container_image_pull: "True"
ccdb_env: {}
ccdb_frontend_rule: "Host:{{ ansible_fqdn }}"

# To use external database host, define 'ccdb_database_host'
# ccdb_database_host: database-host
ccdb_database_container_name: ccdb-database
ccdb_database_image: postgres:9.6.7
ccdb_database_published_ports: []
ccdb_database_volume: ccdb-database
ccdb_database_name: discs_ccdb
ccdb_database_username: ccdb
ccdb_database_password: ccdb
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-ccdb
```

## License

BSD 2-clause
